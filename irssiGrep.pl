#!/usr/bin/perl
use warnings;
use strict;
use vars qw($VERSION %IRSSI);
use Irssi;

$VERSION = '1.0.0';
%IRSSI = (
  authors => 'Viktor Penkov',
  contact => 'vpenkoff@gmail.com',
  name => 'irssiGrep',
  description => 'Use grep like search inside irssi history',
  license     => 'MIT'
);

# TODO:
# Implement:
# -C NUM: print num lines of output context
sub grep_cmd {
  # input - the input from IRSSI including pattern and options
  # log_file - the file where we are going to search
  my ($input, $log_file) = @_;
  my @result = ();

  my $opts = get_opts($input);
  print $opts;

  my $pattern = $opts->{pattern};
  my $regex;

  if ($opts->{opt_i}) {
    $regex = qr/$pattern/i;
  }

  while (<$log_file>) {
    if ($_ =~ $regex) {
      print $_;
    }
  }
}

sub get_opts {
  my $input = shift;
  my @opts = split /\s/, $input;
  my $pattern = pop @opts;
  my $opt_i;
  my $opt_ctx;


  foreach (@opts) {
    if ($_ =~ /^-i$/ ) {
      $opt_i = 1;
    }
    if ($_ =~ /^(-C)=(\d+)$/ ) {
      $opt_ctx = $2;
    }
  }

  my %opts = (
    'opt_i' => $opt_i,
    'opt_ctx' => $opt_ctx,
    'pattern' => $pattern
  );
  return \%opts;
}

sub cmd_grep {
  # data - contains the paramenters for /grep
  # server - the active server in window
  # witem - the active window item (eg. channel, query)
  #         or undef if the window is empty

  my @history = ();
  my ($data, $server, $witem) = @_;

  for my $log (Irssi::logs()) {
    if ($log->{fname} =~ /$witem->{name}/) {
      open(my $fh, "<", $log->{real_fname}) or die "Can't open < " . $log->{real_fname} . ": $!";
      grep_cmd($data, $fh);
      close $fh;
    }
  }
}

Irssi::command_bind('grep', 'cmd_grep');
